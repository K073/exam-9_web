import React from 'react';
import {Col, Grid, PageHeader, Row} from "react-bootstrap";
import Contact from "../../components/Contact/Contact";
import {deleteContact, fetchContact} from "../../store/action";
import {connect} from "react-redux";

class Contacts extends React.Component {
  componentDidMount() {
    this.props.fetchContact();
  }

  componentDidUpdate() {
    console.log(Object.keys(this.props.contacts));
  }

  render() {
    return (
      <Grid>
        <Row>
          <Col>
            <PageHeader>
              Contacts <small>list u contacts</small>
            </PageHeader>
          </Col>
        </Row>
        <Row>
          <Col md={8} mdOffset={2}>
            {Object.keys(this.props.contacts).map(key => {
              return <Contact
                key={key}
                id={key}
                img={this.props.contacts[key].photo}
                name={this.props.contacts[key].name}
                phone={this.props.contacts[key].phone}
                email={this.props.contacts[key].email}
                delete={() => this.props.deleteContact(key)}
              />
            })}
          </Col>
        </Row>
      </Grid>
    )
  }
}

const mapStateToProps = state => {
  return {
    contacts: state.contacts
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchContact: () => dispatch(fetchContact()),
    deleteContact: () => dispatch(deleteContact())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Contacts);