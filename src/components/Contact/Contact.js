import React from 'react';
import profileDefault from '../../assets/profile-default.jpg';
import {Button, Col, Image, Modal, Panel, Row} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

class Contact extends React.Component{
  state = {
    showModal: false
  };

  toggleModal = () => {
    this.setState({showModal: !this.state.showModal})
  };

  render() {
    return (
      <div onClick={this.toggleModal}>
        <Panel>
          <Panel.Heading>{this.props.name}</Panel.Heading>
          <Panel.Body>
            <Row>
              <Col xs={1}>
                <Image style={{maxWidth: '50px'}} src={this.props.img || profileDefault} thumbnail/>
              </Col>
              <Col xs={11}>
                {this.props.phone}
              </Col>
            </Row>
          </Panel.Body>
        </Panel>
        <div hidden={!this.state.showModal} className="static-modal">
          <Modal.Dialog>
            <Modal.Header>
              <Modal.Title>{this.props.name}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <div>{this.props.phone}</div>
              <div>{this.props.email}</div>
              <div><Image style={{maxWidth: '100%'}} src={this.props.img}></Image></div>
            </Modal.Body>
            <Modal.Footer>
              <Button onClick={this.toggleModal}>Close</Button>
              <Button bsStyle="danger" onClick={this.props.delete}>Delete</Button>
              <LinkContainer to={`/edit/${this.props.id}`}>
                <Button bsStyle="primary">Edit Contact</Button>
              </LinkContainer>
            </Modal.Footer>
          </Modal.Dialog>
        </div>;
      </div>
    );
  }
}

export default Contact;