import React from 'react';
import {Button, Col, ControlLabel, FormControl, Grid, Image, PageHeader, Row} from "react-bootstrap";
import {connect} from "react-redux";
import {putContact, sendContact} from "../../store/action";
import axios from '../../axios';
import {LinkContainer} from "react-router-bootstrap";

class CreateContact extends React.Component {
  state = {
    name: '',
    phone: '',
    email: '',
    photo: '',
    edit: false
  };

  inputHandler = event => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  };

  componentDidMount() {
    if (this.props.match.params.id) {
      const contact = this.props.contacts[this.props.match.params.id] ?
        this.props.contacts[this.props.match.params.id] :
        axios.get(`/${this.props.match.params.id}.json`).then(res => {
          return res.data
        });

      this.setState({...contact, edit: true})
    }
  }

  render() {
    return(
      <form>
        <Grid>
          <Row>
            <Col md={12}>
              <PageHeader>
                Create contact
              </PageHeader>;
            </Col>
          </Row>
          <Row bsStyle={'padding-vertical'}>
            <Col md={6} mdOffset={3}>
              <ControlLabel>Name</ControlLabel>
              <FormControl
                name={'name'}
                value={this.state.name}
                onChange={this.inputHandler}
              />
            </Col>
          </Row>
          <Row bsStyle={'padding-vertical'}>
            <Col md={6} mdOffset={3}>
              <ControlLabel>Phone</ControlLabel>
              <FormControl
                name={'phone'}
                value={this.state.phone}
                onChange={this.inputHandler}
              />
            </Col>
          </Row>
          <Row bsStyle={'padding-vertical'}>
            <Col md={6} mdOffset={3}>
              <ControlLabel>Email</ControlLabel>
              <FormControl
                name={'email'}
                value={this.state.email}
                onChange={this.inputHandler}
              />
            </Col>
          </Row>
          <Row bsStyle={'padding-vertical'}>
            <Col md={6} mdOffset={3}>
              <ControlLabel>Photo</ControlLabel>
              <FormControl
                name={'photo'}
                value={this.state.photo}
                onChange={this.inputHandler}
              />
              <Image style={{maxWidth: '100%'}} src={this.state.photo} rounded />
            </Col>
          </Row>
          <Row bsStyle={'padding-vertical'}>
            <Col md={6} mdOffset={3}>
              <Button onClick={() => {
                this.state.edit ? this.props.updateContact({
                  name: this.state.name,
                  phone: this.state.phone,
                  email: this.state.email,
                  photo: this.state.photo
                }, this.props.match.params.id).then(() => {
                  this.props.history.goBack();
                }) : this.props.sendContact({
                  name: this.state.name,
                  phone: this.state.phone,
                  email: this.state.email,
                  photo: this.state.photo
                }).then(() => {
                  this.props.history.push('/')
                })
              }} bsStyle="success">Save</Button>
              <LinkContainer to={'/'} exact>
                <Button className={'pull-right'} bsStyle="warning">Back to contacts</Button>
              </LinkContainer>
            </Col>
          </Row>
        </Grid>
      </form>
    );
  }
}

const mapStateToProps = state => {
  return {
    contacts: state.contacts
  };
};

const mapDispatchToProps = dispatch => {
  return {
    sendContact: (value) => dispatch(sendContact(value)),
    updateContact: (value, id) => dispatch(putContact(value, id))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateContact);