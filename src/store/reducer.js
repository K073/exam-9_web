import {FETCH_CONTACT_SUCCESS} from "./action";

const initState = {
  contacts: {}
};

const reducer = (state = initState, action) => {
  switch (action.type) {
    case FETCH_CONTACT_SUCCESS:
      return {...state, contacts: action.data};
    default:
      return state;
  }
};

export default reducer;